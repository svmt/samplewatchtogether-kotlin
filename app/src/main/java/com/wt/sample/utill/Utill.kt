package com.wt.sample.utill

import android.content.Context
import android.widget.Toast
import com.wt.sample.R
import java.util.*
import java.util.concurrent.TimeUnit

class Utill {
    companion object {
        fun showError(context: Context, errorMessage: String?) {
            showToast(context, errorMessage, android.R.color.holo_red_dark)
        }

        fun showMessage(context: Context, errorMessage: String?) {
            showToast(context, errorMessage, R.color.colorAccent)
        }

        private fun showToast(context: Context, errorMessage: String?, textColorId: Int) {
            val t = Toast.makeText(context, errorMessage, Toast.LENGTH_LONG)
            t.show()
        }

        fun formatDurationToHHMMSSSSS(duration: Long): String? {
            val seconds = TimeUnit.MILLISECONDS.toSeconds(duration)
            val remainingSeconds = Math.abs(seconds % 60)
            val mins = seconds / 60
            val remainingMins = Math.abs(mins % 60)
            val hours = mins / 60
            return String.format(
                Locale.US,
                "%d:%02d:%02d",
                hours,
                remainingMins,
                remainingSeconds
            )
        }
    }
}