package com.wt.sample.ui

import android.Manifest.permission.CAMERA
import android.Manifest.permission.RECORD_AUDIO
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.pixplicity.easyprefs.library.Prefs
import com.wt.sample.BuildConfig
import com.wt.sample.R
import com.wt.sample.utill.Utill


class StartActivity : Activity() {

    companion object {
        const val DISPLAY_NAME_CODE = "display_name_code"
    }

    private lateinit var mDisplayName: AppCompatEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        (findViewById<View>(R.id.app_version) as AppCompatTextView).text = BuildConfig.VERSION_NAME

        mDisplayName = findViewById(R.id.display_name)
        mDisplayName.setText(Prefs.getString(DISPLAY_NAME_CODE, ""))

        // Click Listener for sign in button
        (findViewById<Button>(R.id.sign_in)).setOnClickListener {
            checkMediaPermissions()
        }

        // Done actions sign in button click
        mDisplayName.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    checkMediaPermissions()
                    true
                }
                else -> false
            }
        }
    }

    private fun checkMediaPermissions() {
        if (ContextCompat.checkSelfPermission(this, RECORD_AUDIO) +
            ContextCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(RECORD_AUDIO, CAMERA), 1)
            return
        }
        // Permissions granted
        signIn()
    }

    // Check if user granted media permissions for app
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[0] == grantResults[1]) {
            signIn()
            return
        }
        // Permissions not allowed
        Utill.showError(this, "Camera and Microphone permissions not allowed")
    }

    private fun signIn() {
        val displayName = mDisplayName.text.toString()
        Prefs.putString(DISPLAY_NAME_CODE, displayName)
        startMainActivity(displayName)
    }

    private fun startMainActivity(
        displayName: String
    ) {
        val intent =
            Intent(this@StartActivity, MainActivity::class.java)
        intent.putExtra(DISPLAY_NAME_CODE, displayName)
        // Start MainActivity with params
        startActivity(intent)
    }

    private fun onError(error: String?) {
        Utill.showError(this, error)
    }
}