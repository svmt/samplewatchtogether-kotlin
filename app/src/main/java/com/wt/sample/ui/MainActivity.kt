package com.wt.sample.ui

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.wt.sample.R
import com.wt.sample.utill.Utill
import com.wt.sdk.*
import com.wt.sdk.MediaConfiguration.MediaState
import com.wt.sdk.Session.SessionBuilder
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MainActivity : AppCompatActivity(), SessionListener, ReconnectListener, ConnectionListener,
    PublishersAdapter.SwitchCamCallback {

    private var mSession: Session? = null
    private var mPublisherAdapter: PublishersAdapter? = null
    private var mParticipantAdapter: ParticipantsAdapter? = null
    private var mDisplayName = ""
    private var mToken = ""
    private var mReconnectTimer: Timer? = null
    private var mCountTimer = Timer("MainActivityCountThread")
    private var mConnectionStateTimer = Timer("MainActivityCountThread")
    private var mHandler = Handler()
    private var mSessionTimeDurationMls: Long = 0

    private lateinit var mAudioQos: AppCompatTextView
    private lateinit var mVideoQos: AppCompatTextView
    private lateinit var mMode: AppCompatTextView
    private lateinit var mSessionTimeDuration: AppCompatTextView
    private lateinit var mConnectionState: AppCompatTextView
    private lateinit var mSwitchMode: SwitchCompat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // To prevent screen to be switched off
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        // Extract session's params
        if (intent != null) {
            if (intent.extras != null) {
                mDisplayName = intent.extras!!.get(StartActivity.DISPLAY_NAME_CODE).toString()
            }
        }

        initViewsAndListeners()
        initSession()
    }

    private fun initViewsAndListeners() {
        val btnStartCameraPreview = findViewById<AppCompatButton>(R.id.btn_cam_preview)
        val btnConnectSessionFP = findViewById<AppCompatButton>(R.id.btn_connect_session_fp)
        val btnConnectSessionAVB = findViewById<AppCompatButton>(R.id.btn_connect_session_avb)
        val btnConnectSessionAB = findViewById<AppCompatButton>(R.id.btn_connect_session_ab)
        val btnConnectWatching = findViewById<AppCompatButton>(R.id.btn_connect_watching)
        val btnAdjustVideoQuality = findViewById<AppCompatButton>(R.id.btn_adjust_frame_rate)
        val btnFPType = findViewById<AppCompatButton>(R.id.btn_pt_fp)
        val btnAVBType = findViewById<AppCompatButton>(R.id.btn_pt_avb)
        val txtWidth = findViewById<AppCompatTextView>(R.id.txt_width_value)
        val txtHeight = findViewById<AppCompatTextView>(R.id.txt_height_value)
        val txtFrameRate = findViewById<AppCompatTextView>(R.id.txt_frame_rate_value)
        mAudioQos = findViewById(R.id.txt_audio_qos_value)
        mVideoQos = findViewById(R.id.txt_video_qos_value)
        mMode = findViewById(R.id.txt_service_mode_value)
        mSwitchMode = findViewById(R.id.switch_service_mode)
        mSessionTimeDuration = findViewById(R.id.txt_session_time_duration)
        mConnectionState = findViewById(R.id.txt_connection_state)
        val widthSeek = findViewById<SeekBar>(R.id.seek_width)
        val heightSeek = findViewById<SeekBar>(R.id.seek_height)
        val frameSeek = findViewById<SeekBar>(R.id.seek_frame_rate)
        setParticipantsTypesSheet()
        txtWidth.text = widthSeek.progress.toString()
        txtHeight.text = heightSeek.progress.toString()
        txtFrameRate.text = frameSeek.progress.toString()

        mSwitchMode.setOnCheckedChangeListener { compoundButton, b ->
            val txtMode =
                getString(if (b) R.string.txt_mode_mesh else R.string.txt_mode_sfu)
            mMode.text = txtMode
        }

        btnStartCameraPreview.setOnClickListener {
            btnStartCameraPreview.visibility = View.GONE
            mSession?.setVideoResolution(widthSeek.progress, heightSeek.progress)
            mSession?.setVideoFrameRate(frameSeek.progress)
            mSession?.startCameraPreview()
        }

        btnConnectSessionFP.setOnClickListener {
            btnStartCameraPreview.visibility = View.GONE
            btnConnectSessionFP.visibility = View.GONE
            btnConnectSessionAVB.visibility = View.GONE
            btnConnectSessionAB.visibility = View.GONE
            btnConnectWatching.visibility = View.GONE
            mSwitchMode.visibility = View.GONE
            setSessionParams()
            mSession?.setVideoResolution(widthSeek.progress, heightSeek.progress)
            mSession?.setVideoFrameRate(frameSeek.progress)
            mSession?.connect()
        }

        btnConnectSessionAVB.setOnClickListener {
            btnStartCameraPreview.visibility = View.GONE
            btnConnectSessionFP.visibility = View.GONE
            btnConnectSessionAVB.visibility = View.GONE
            btnConnectSessionAB.visibility = View.GONE
            btnConnectWatching.visibility = View.GONE
            mSwitchMode.visibility = View.GONE
            setSessionParams()
            mSession?.setVideoResolution(widthSeek.progress, heightSeek.progress)
            mSession?.setVideoFrameRate(frameSeek.progress)
            mSession?.connectWithAudio()
        }

        btnConnectSessionAB.setOnClickListener {
            btnStartCameraPreview.visibility = View.GONE
            btnConnectSessionFP.visibility = View.GONE
            btnConnectSessionAVB.visibility = View.GONE
            btnConnectSessionAB.visibility = View.GONE
            btnConnectWatching.visibility = View.GONE
            mSwitchMode.visibility = View.GONE
            setSessionParams()
            mSession?.connectWithAudio()
        }

        btnConnectWatching.setOnClickListener {
            btnStartCameraPreview.visibility = View.GONE
            btnConnectSessionFP.visibility = View.GONE
            btnConnectSessionAVB.visibility = View.GONE
            btnConnectSessionAB.visibility = View.GONE
            btnConnectWatching.visibility = View.GONE
            mSwitchMode.visibility = View.GONE
            setSessionParams()
            mSession?.connectAsViewer()
        }

        btnFPType.setOnClickListener {
            BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet)).state =
                BottomSheetBehavior.STATE_COLLAPSED
            mSession?.setLocalParticipantType(ParticipantType.FULL_PARTICIPANT)
        }

        btnAVBType.setOnClickListener {
            BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet)).state =
                BottomSheetBehavior.STATE_COLLAPSED
            mSession?.setLocalParticipantType(ParticipantType.AV_BROADCASTER)
        }

        btnAdjustVideoQuality.setOnClickListener {
            mSession?.adjustResolution(widthSeek.progress, heightSeek.progress)
            mSession?.adjustFrameRate(frameSeek.progress)
        }

        widthSeek.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(
                    seekBar: SeekBar,
                    progress: Int,
                    fromUser: Boolean
            ) {
                if (fromUser) {
                    val widthProgress: Int = getResolutionProgress(progress)
                    widthSeek.progress = widthProgress
                    txtWidth.text = widthProgress.toString()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        heightSeek.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(
                    seekBar: SeekBar,
                    progress: Int,
                    fromUser: Boolean
            ) {
                if (fromUser) {
                    val heightProgress: Int = getResolutionProgress(progress)
                    heightSeek.progress = heightProgress
                    txtHeight.text = heightProgress.toString()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        frameSeek.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(
                    seekBar: SeekBar,
                    progress: Int,
                    fromUser: Boolean
            ) {
                if (fromUser) {
                    val frameRateProgress: Int = when {
                        progress < 5 -> 1
                        progress < 10 -> 5
                        progress < 15 -> 10
                        progress < 20 -> 15
                        progress < 25 -> 20
                        progress < 30 -> 25
                        progress < 40 -> 30
                        progress < 50 -> 40
                        progress < 60 -> 50
                        else -> 60
                    }
                    frameSeek.progress = frameRateProgress
                    txtFrameRate.text = frameRateProgress.toString()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        // Create ParticipantsAdapter instance
        mPublisherAdapter = PublishersAdapter(this)
        // Set ParticipantsAdapter to RecyclerView
        findViewById<RecyclerView>(R.id.publishers).adapter = mPublisherAdapter

        // Create SubscriberAdapter instance
        mParticipantAdapter = ParticipantsAdapter()
        // Set PublisherAdapter to RecyclerView
        (findViewById<View>(R.id.participants) as RecyclerView).adapter = mParticipantAdapter
    }

    private fun setParticipantsTypesSheet() {
        BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet)).state =
            BottomSheetBehavior.STATE_COLLAPSED
        BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet)).isDraggable =
            true

        //click event for show-dismiss bottom sheet
        findViewById<View>(R.id.sheet_header).setOnClickListener {
            if (BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet))
                    .state != BottomSheetBehavior.STATE_EXPANDED
            ) {
                BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet))
                    .setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet))
                    .setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
        }
        findViewById<View>(R.id.participants_types_sheet).visibility = View.GONE
    }

    private fun getResolutionProgress(progress: Int): Int {
        return when {
            progress < 144 -> 120
            progress < 160 -> 144
            progress < 176 -> 160
            progress < 240 -> 176
            progress < 288 -> 240
            progress < 320 -> 288
            progress < 352 -> 320
            progress < 360 -> 352
            progress < 480 -> 360
            progress < 600 -> 480
            progress < 640 -> 600
            progress < 720 -> 640
            progress < 768 -> 720
            progress < 800 -> 768
            progress < 1024 -> 800
            progress < 1200 -> 1024
            progress < 1280 -> 1200
            else -> 1280
        }
    }

    private fun initSession() {
        mSession = SessionBuilder(this)
            .setReconnectListener(this)
            .setConnectionListener(this)
            .build(this)
    }

    private fun setSessionParams() {
        mSession?.setDisplayName(mDisplayName)
        mSession?.setToken(mToken)
        mSession?.setServiceMode(mSwitchMode.isChecked)
        mSession?.enableStats()
    }

    override fun onPause() {
        super.onPause()
        disconnect()
    }

    override fun onBackPressed() {
        if (BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet))
                .state == BottomSheetBehavior.STATE_EXPANDED
        ) {
            BottomSheetBehavior.from(findViewById<View>(R.id.participants_types_sheet)).state =
                BottomSheetBehavior.STATE_COLLAPSED
            return
        }
        disconnect()
        super.onBackPressed()
    }

    private fun disconnect() {
        clearAdapters()
        mSession?.disconnect()
    }

    private fun clearAdapters() {
        // Clear participants and disconnect from the session
        mPublisherAdapter?.clearPublishers()
        mParticipantAdapter?.clearParticipants()
    }

    override fun onDestroy() {
        clearAdapters()
        super.onDestroy()
    }

    override fun onConnected(sessionId: String?, participants: ArrayList<String>?) {
        Utill.showMessage(this, "Connected to session with id:\n$sessionId")
        findViewById<View>(R.id.participants_types_sheet).visibility = View.VISIBLE
        mSessionTimeDuration.visibility = View.VISIBLE
        mCountTimer.schedule(object : TimerTask() {
            override fun run() {
                mSessionTimeDurationMls += 1000
                if (isFinishing) {
                    cancel()
                    return
                }
                mHandler.post {
                    mSessionTimeDuration.text = Utill.formatDurationToHHMMSSSSS(mSessionTimeDurationMls)
                }
                if (mSession != null) {
                    mSession!!.sendPlayerData(mSessionTimeDurationMls.toString())
                }
            }
        }, 0, 1000)
    }

    override fun onOrganizerReceived() {
        // Method is triggered when user becomes an organizer of the Session.
        Utill.showMessage(this, "You got organizer session's role")
    }

    override fun onDisconnected() {
        Utill.showError(this, "Disconnected from session")
        finish()
    }

    override fun onError(error: SessionError?) {
        if (error != null && error.name.isNotEmpty()) {
            var errStr = error.name
            if (error.description.isNotEmpty()) {
                errStr += " $error.description"
            }
            Utill.showError(this, errStr)
        }
    }

    override fun onRemoteParticipant(participant: Participant?) {
        if (participant != null) {
            mParticipantAdapter?.addParticipant(participant.name)
        }
    }

    override fun onParticipantLeft(participant: Participant?) {
        if (participant != null) {
            mParticipantAdapter?.removeParticipant(participant.name)
        }
    }

    override fun onLocalPublisher(publisher: Participant?) {
        if (publisher != null) {
            mPublisherAdapter?.addLocalPublisher(publisher)
        }
    }

    override fun onRemotePublisher(publisher: Participant?) {
        if (publisher != null) {
            mPublisherAdapter?.addPublisher(publisher)
        }
    }

    override fun onUpdatePublisher(participantId: String?, participant: Participant?) {
        mReconnectTimer?.cancel()
        if (participant != null) {
            mPublisherAdapter?.updatePublisher(participantId, participant)
        }
    }

    override fun onPublisherLeft(publisher: Participant?) {
        if (publisher != null) {
            mPublisherAdapter?.removePublisher(publisher)
        }
    }

    override fun onPublisherMediaStateChanged(
            participantId: String?,
            mediaType: MediaConfiguration.MediaType?,
            mediaState: MediaState?
    ) {
        mPublisherAdapter?.updatePublisherMedia(participantId, mediaType, mediaState)
    }

    override fun onStatsReceived(stats: JSONObject?) {
        if (stats != null) {
            try {
                mAudioQos.text =
                    stats["audioAvg"].toString() + " -> " + stats["audioQosMos"].toString()
                mVideoQos.text =
                    stats["videoAvg"].toString() + " -> " + stats["videoQosMos"].toString()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun onStateServiceMode(mode: String?) {
        mMode.text = mode
    }

    override fun onPlayerDataReceived(data: String?) {
        // Method is triggered when user gets sync notification from session's organizer.
        Utill.showMessage(this, "Player data received = $data")
        // Set player speed according delta
    }

    override fun onSwitchCameraClicked() {
        mSession?.switchCamera()
    }

    override fun onLocalParticipantReconnecting() {
        Utill.showMessage(this, "Reconnecting local participant...")
    }

    override fun onLocalParticipantReconnected() {
        Utill.showMessage(this, "Reconnected local participant...")
    }

    override fun onRemoteParticipantReconnecting(participant: Participant?, participantId: String?) {
        mPublisherAdapter?.progressConnection(participantId, true)
        mReconnectTimer = Timer("MainActivityReconnectThread")
        mReconnectTimer?.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    mPublisherAdapter?.removePublisherById(participantId)
                    mParticipantAdapter?.removeParticipant(participant?.name)
                }
            }
        }, 7000)
    }

    override fun onRemoteParticipantReconnected(oldParticipantId: String?, participant: Participant?) {
        mReconnectTimer?.cancel()
        mPublisherAdapter?.progressConnection(oldParticipantId, false)
    }

    override fun onLocalConnectionLost() {
        setConnectionState(R.string.txt_connection_lost, false)
    }

    override fun onLocalConnectionResumed() {
        setConnectionState(R.string.txt_connection_resumed, true)
    }

    override fun onRemoteConnectionLost(participantId: String?) {
        mPublisherAdapter?.remotePublisherConnectionLost(participantId)
    }

    private fun setConnectionState(strConnectionState: Int, isConnected: Boolean) {
        val txtColor: Int = if (isConnected) R.color.colorGreen else R.color.colorRed
        mConnectionState.visibility = View.VISIBLE
        mConnectionState.setText(strConnectionState)
        mConnectionState.setBackgroundColor(ContextCompat.getColor(this, txtColor))
        mConnectionStateTimer.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread { mConnectionState.visibility = View.GONE }
            }
        }, 3000)
    }
}