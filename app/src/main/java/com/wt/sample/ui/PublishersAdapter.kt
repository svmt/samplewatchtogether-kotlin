package com.wt.sample.ui

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.wt.sample.R
import com.wt.sample.ui.PublishersAdapter.ViewHolder
import com.wt.sdk.MediaConfiguration
import com.wt.sdk.MediaConfiguration.MediaState
import com.wt.sdk.Participant
import com.wt.sdk.ParticipantType
import com.wt.sdk.VideoRenderer
import java.util.*

class PublishersAdapter(private val mSwitchCamCallback: SwitchCamCallback) :
    RecyclerView.Adapter<ViewHolder>() {

    private val mPublishers = ArrayList<Participant>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.item_publisher,
                viewGroup,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val participant = mPublishers[viewHolder.adapterPosition]
        participant.videoRenderer = viewHolder.videoRenderer
        // Attaching participant's media stream to video renderer
        participant.attachStream()
        viewHolder.mic.setOnClickListener {
            if (participant.isAudioEnabled) {
                participant.disableAudio()
            } else {
                participant.enableAudio()
            }
        }
        viewHolder.cam.setOnClickListener {
            if (participant.isVideoEnabled) {
                participant.disableVideo()
            } else {
                participant.enableVideo()
            }
        }
        viewHolder.connectionState.setVisibility(if (participant.isConnectionLost) View.VISIBLE else View.GONE)
        viewHolder.layoutProgress.setVisibility(if (participant.isProgressReconnection) View.VISIBLE else View.GONE)
        viewHolder.mic.setBackgroundResource(if (participant.isAudioEnabled) R.drawable.ic_mic_on else R.drawable.ic_mic_off)
        viewHolder.cam.setBackgroundResource(if (participant.isVideoEnabled) R.drawable.ic_video_on else R.drawable.ic_video_off)

        viewHolder.name.text = participant.name

        // Check if its local participant

        // Check if its local participant
        viewHolder.switchCam.visibility = if (participant.isLocal) View.VISIBLE else View.GONE
        viewHolder.volumeLevel.visibility = if (participant.isLocal) View.GONE else View.VISIBLE

        // Check if its local participant
        if (viewHolder.adapterPosition == 0) {
            viewHolder.switchCam.visibility = View.VISIBLE
            viewHolder.volumeLevel.visibility = View.GONE
            viewHolder.switchCam.setOnClickListener {
                mSwitchCamCallback.onSwitchCameraClicked()
            }
        } else {
            viewHolder.volumeLevel.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    if (fromUser) {
                        participant.setVolumeLevel(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })
        }
    }

    override fun getItemCount(): Int {
        return mPublishers.size
    }

    fun remotePublisherConnectionLost(participantId: String?) {
        for (i in mPublishers.indices) {
            val participant = mPublishers[i]
            if (participant.id == participantId) {
                val itemIndex = mPublishers.indexOf(participant)
                participant.isConnectionLost = true
                notifyItemChanged(itemIndex, participant)
            }
        }
    }

    fun updatePublisher(publisherId: String?, publisher: Participant) {
        for (i in mPublishers.indices) {
            val participant = mPublishers[i]
            if (participant.id == publisherId) {
                val itemIndex = mPublishers.indexOf(participant)
                publisher.videoRenderer = participant.videoRenderer
                publisher.isStreamAttached = false
                publisher.isUpdateVideoRenderer = true
                publisher.isAudioLocalEnabled = participant.isAudioLocalEnabled
                publisher.isVideoLocalEnabled = participant.isVideoLocalEnabled
                publisher.isAudioEnabled = participant.isAudioEnabled
                publisher.isVideoEnabled = participant.isVideoEnabled
                publisher.isConnectionLost = false
                publisher.attachStream()
                mPublishers[itemIndex] = publisher
                notifyItemChanged(itemIndex, publisher)
            }
        }
    }

    fun progressConnection(
        publisherId: String?,
        isProgress: Boolean
    ) {
        for (i in mPublishers.indices) {
            val participant = mPublishers[i]
            if (participant.id == publisherId) {
                val itemIndex = mPublishers.indexOf(participant)
                participant.isProgressReconnection = isProgress
                mPublishers[itemIndex] = participant
                notifyItemChanged(itemIndex, participant)
            }
        }
    }

    fun addLocalPublisher(participant: Participant) {
        if (mPublishers.size > 0) {
            if (mPublishers[0].isLocal) {
                removePublisher(mPublishers[0])
            }
        }
        addPublisher(participant)
    }

    internal fun addPublisher(participant: Participant) {
        if (participant.type == ParticipantType.FULL_PARTICIPANT || participant.type == ParticipantType.AV_BROADCASTER) {
            mPublishers.add(participant)
            notifyItemInserted(itemCount - 1)
        }
    }

    internal fun removePublisher(participant: Participant) {
        if (mPublishers.isNotEmpty() && mPublishers.contains(participant)) {
            val positionToRemove = mPublishers.indexOf(participant)
            mPublishers.remove(participant)
            notifyItemRemoved(positionToRemove)
            participant.releaseRenderer()
        }
    }

    fun removePublisherById(publisherId: String?) {
        for (participant in mPublishers) {
            if (participant.id == publisherId) {
                val positionToRemove = mPublishers.indexOf(participant)
                mPublishers.remove(participant)
                notifyItemRemoved(positionToRemove)
                participant.releaseRenderer()
                break
            }
        }
    }

    fun clearPublishers() {
        for (participant in mPublishers) {
            if (participant.videoRenderer != null) {
                participant.releaseRenderer()
            }
        }
    }

    fun updatePublisherMedia(
        participantId: String?,
        mediaType: MediaConfiguration.MediaType?,
        mediaState: MediaState?
    ) {
        if (mPublishers.isNotEmpty()) {
            for (position: Int in mPublishers.indices) {
                val participant: Participant = mPublishers[position]
                if (!TextUtils.isEmpty(participant.id) &&
                    !TextUtils.isEmpty(participantId) && participant.id == participantId) {
                    when (mediaType) {
                        MediaConfiguration.MediaType.AUDIO -> participant.setAudioEnabled(
                            mediaState
                        )
                        MediaConfiguration.MediaType.VIDEO -> participant.setVideoEnabled(
                            mediaState
                        )
                        else -> {
                        }
                    }
                    notifyItemChanged(
                        position,
                        participant
                    ) // Update exactly view after its changed
                    break
                }
            }
        }
    }

    interface SwitchCamCallback {
        fun onSwitchCameraClicked()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val layoutProgress: LinearLayout = v.findViewById(R.id.layout_progress)
        val videoRenderer: VideoRenderer = v.findViewById(R.id.video_renderer)
        val name: AppCompatTextView = v.findViewById(R.id.participant_name)
        val connectionState: AppCompatTextView = v.findViewById(R.id.txt_connection_state)
        val mic: AppCompatImageView = v.findViewById(R.id.mic)
        val cam: AppCompatImageView = v.findViewById(R.id.cam)
        val switchCam: AppCompatImageView = v.findViewById(R.id.switch_cam)
        val volumeLevel: SeekBar = v.findViewById(R.id.volume_level)
    }
}