package com.wt.sample.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.wt.sample.R
import java.util.*

class ParticipantsAdapter : RecyclerView.Adapter<ParticipantsAdapter.ViewHolder>() {
    private val mParticipants = ArrayList<String>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.item_participant,
                viewGroup,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.name.text = mParticipants[viewHolder.adapterPosition]
        viewHolder.order.text = (viewHolder.adapterPosition + 1).toString()
    }

    override fun getItemCount(): Int {
        return mParticipants.size
    }

    fun addParticipant(participantName: String) {
        mParticipants.add(participantName)
        notifyItemInserted(itemCount)
    }

    fun removeParticipant(participantName: String?) {
        if (mParticipants.isNotEmpty() && mParticipants.contains(participantName)) {
            val positionToRemove = mParticipants.indexOf(participantName)
            mParticipants.remove(participantName)
            notifyItemRemoved(positionToRemove)
        }
    }

    internal fun clearParticipants() {
        mParticipants.clear()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name: AppCompatTextView = v.findViewById(R.id.name)
        val order: AppCompatTextView = v.findViewById(R.id.order)
    }
}